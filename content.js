// alert("injecting this into the page");


let divElement = document.createElement("Div");
divElement.id = "divID";

divElement.setAttribute('class','mydiv');
divElement.style.textAlign = "center";
divElement.style.fontWeight = "bold";
divElement.style.fontSize = "smaller";


let paragraph = document.createElement("P");
let text = document.createTextNode("Hello guys we have injected code in this page");
paragraph.appendChild(text);
divElement.appendChild(paragraph);

// Adding a button, cause why not!
let button = document.createElement("Button");
button.setAttribute('class','button');
let textForButton = document.createTextNode("Release the alert");
button.appendChild(textForButton);
button.addEventListener("click", function(){
    alert("Hello from JS-Meet Up!");
});
divElement.appendChild(button);

// Appending the div element to body
document.body.appendChild(divElement);