// 447a43c737a34194a21326a34010476f



//
console.log("Background running");



window.addEventListener("load", function (event) {
    fetch("https://newsapi.org/v2/top-headlines?country=us&category=technology&apiKey=447a43c737a34194a21326a34010476f").then(res =>{
        res.json().then(data => {
            let articles = data["articles"];

            document.getElementById("loader").style.display = "none";

            // alert(JSON.stringify(articles));
            let newsContainer = document.getElementById("news");

            articles.forEach((item, index)=>{

                newsContainer.innerHTML  += `
           <div  class="news-item">
            <img class="news-img" src=${item["urlToImage"]} alt="news-img"/>
            <div class="news-details">
                <h3 class="title">${item["title"]} </h3>
                <span class="desc">${item["description"]}</span>
            </div>
            <div style="margin:2px 8px">
            <span style="color: darkgray;">Date Published <span style="color:#282828">${item["publishedAt"]}</span></span>
            </div>
           <a href=${item["url"]} target="_blank">
                <button id="read-more" class="read-more">
                read more
                </button>
           </a>

        </div>`;

            })
        })
    });

    let nav = document.getElementById("nav-menu");

    document.getElementById("nav").addEventListener("click", function () {
        console.log();
        if(nav.style.display === "flex"){
            nav.style.display = 'none';
        }else{
            nav.style.display = 'flex'
        }

    });

});


